<?php
// As inspired by: https://theswedishbear.com/woocommerce-change-all-email-titles-in-wc-admin/

namespace WPezSuite\WCezClasses\AdminEmailTitles;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassAdminEmailTitles' ) ) {
    class ClassAdminEmailTitles implements InterfaceAdminEmailTitles {

        protected $_arr_email_id;
        protected $_arr_new_titles;


        public function __construct() {

            $this->setPropertyDefaults();
        }

        protected function setPropertyDefaults() {

            $this->_arr_new_titles = [];

            $this->_arr_email_id = [
                'new_order',
                'cancelled_order',
                'failed_order',
                'customer_on_hold_order',
                'customer_processing_order',
                'customer_completed_order',
                'customer_refunded_order',
                'customer_invoice',
                'customer_note',
                'customer_reset_password',
                'customer_new_account',
            ];

        }


        public function pushTitle( $str_email_id = false, $str_value = false ) {

            if ( isset( $this->_arr_email_id[ $str_email_id ] ) && is_string( $str_value ) ) {

                $this->_arr_email_id[ $str_email_id ] = $str_value;

                return true;

            }

            return false;
        }


        /**
         * @param $str_title
         * @param $obj_email
         *
         * @return mixed
         */
        public function filterWooCommerceEmailTitle( $str_title, $obj_email ) {


            if ( ! isset( $_GET['page'], $_GET['tab'], $obj_email->id ) || $_GET['page'] !== 'wc-settings' || $_GET['tab'] !== 'email' ) {
                return $str_title;
            }

            if ( isset( $this->_arr_new_titles[ $obj_email->id ] ) ) {
                return $this->_arr_new_titles[ $obj_email->id ];
            }
            return $str_title;

        }
    }
}