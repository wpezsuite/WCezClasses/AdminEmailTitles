## WCezClasses: WooCommerce Admin Email Titles

__A quick and convenient way to change the WooCommerce admin emails titles, and do it The ezWay.__ 

As inspired by: https://theswedishbear.com/woocommerce-change-all-email-titles-in-wc-admin/


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WCezClasses autoloader (link below)._

```
use WPezSuite\WCezClasses\AdminEmailTitles\ClassAdminEmailTitles as AET;
use WPezSuite\WCezClasses\AdminEmailTitles\ClassHooks as Hooks;

$new_aet = new AET();
$new_aet->pushTitle( 'new_order', 'Order:  New' );
$new_aet->pushTitle( 'cancelled_order', 'Order:  Cancelled' );

$new_hooks = new Hooks($new_aet);
$new_hooks->register();
```



### HELPFUL LINKS
 
 - https://gitlab.com/WPezSuite/WCezClasses/WCezAutoload
 
 - https://theswedishbear.com/woocommerce-change-all-email-titles-in-wc-admin/
 
  
### TODO

n/a 


### CHANGE LOG



- v0.0.3 - Friday 3 May 2019
    - Forked from WPezClasses
    - UPDATED: file and class names to match move to WCezClasses

- v0.0.2 - Monday 29 April 2019
    - UPDATED: readme (example). no change to code

- v0.0.2 - Monday 22 April 2019
    - UPDATED: interface file / name

- v0.0.1 - 18 April 2019
    - Hey! Ho!! Let's go!!! 