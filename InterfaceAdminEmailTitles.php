<?php

namespace WPezSuite\WCezClasses\AdminEmailTitles;


interface InterfaceAdminEmailTitles {

	public function filterWooCommerceEmailTitle( $str_title, $obj_email );

}
